import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { GetUserDto } from './dto/get-user.dto';

const MAX_USERS = 5;

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  public async createUser(createUserDto: CreateUserDto): Promise<void> {
    try {
      const users = await this.findAllUsers();
      if (users.length === MAX_USERS) {
        const lastUser = users[MAX_USERS - 1];
        if (lastUser.score < createUserDto.score) {
          await this.deleteUser(lastUser.id);
          await this.userRepository.save(createUserDto);
        }
      } else {
        await this.userRepository.save(createUserDto);
      }
    } catch (e) {
      throw e;
    }
  }

  public async findAllUsers(): Promise<GetUserDto[]> {
    try {
      const users = await this.userRepository
        .createQueryBuilder('user')
        .orderBy('user.score', 'DESC')
        .getMany();
      return users;
    } catch (e) {
      throw e;
    }
  }

  public async deleteUser(id: string): Promise<void> {
    try {
      await this.userRepository.delete(id);
    } catch (e) {
      throw e;
    }
  }
}
