import {
  IsInt,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, {
    message: 'The name must contain a minimum of three characters',
  })
  @MaxLength(15, {
    message: 'The name should not be more than fifteen characters',
  })
  name: string;

  @IsNotEmpty()
  @IsInt()
  score: number;
}
