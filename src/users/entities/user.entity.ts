import { CoreEntity } from '../../application/entities/core.entity';
import { Column, Entity, Index } from 'typeorm';

@Entity({ name: 'users' })
export class User extends CoreEntity {
  @Column({
    type: 'varchar',
    length: 15,
    nullable: false,
  })
  name: string;

  @Index()
  @Column({
    type: 'integer',
    nullable: false,
  })
  score: number;
}
