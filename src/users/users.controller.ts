import { Body, Controller, Get, Post } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { GetUserDto } from './dto/get-user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  @Post('/')
  createUser(@Body() createUserDto: CreateUserDto): Promise<void> {
    console.log(createUserDto);
    return this.usersService.createUser(createUserDto);
  }

  @Get('/')
  findAllUsers(): Promise<GetUserDto[]> {
    return this.usersService.findAllUsers();
  }
}
