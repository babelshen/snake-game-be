import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUserTable1707481813662 implements MigrationInterface {
  name = 'CreateUserTable1707481813662';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying(15) NOT NULL, "score" integer NOT NULL, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_43e8ceaf318680ec7981ea99d7" ON "users" ("score") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP INDEX "public"."IDX_43e8ceaf318680ec7981ea99d7"`,
    );
    await queryRunner.query(`DROP TABLE "users"`);
  }
}
